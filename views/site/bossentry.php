<?php
//VIEW для представления ввода данных о Биг Боссе

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Staff;

?>

	<div class="jumbotron">
		<h2>Отдел кадров компании  'Забота'</h2>

		<p class="lead">Давайте введем вашего самого главного начальника</p>
		<h5>(При необходимости его данные можно корректировать позже из общей структуры компании)</h5>

	</div>
<?php
//  Предоставляем формат ввода для данных о ББ

	$model->firstname = 'Иосиф'  ;
	$model->lastname = 'Сталин'  ;
	$model->position = 'Генеральный секретарь ЦК ВКП(б)'  ;
	$model->email = 'josyp@domain.com'  ;
	$model->homephone = '+001 001-01-01'  ;
	$model->parentid = -1 ;
	$model->parentname = 'Нет' ;
	$model->notes = 'Рельно крутой управленец. Проявляя заботу о людях может привести компанию к невиданному успеху'  ;

?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'firstname')->label('Имя самого главного начальника'); ?>
<?= $form->field($model, 'lastname')->label('Фамилия самого главного начальника'); ?>
<?= $form->field($model, 'position')->label('Как правильно называется его должность'); ?>
<?= $form->field($model, 'email')->label('E-mail'); ?>
<?= $form->field($model, 'homephone')->label('Номер домашнего телефона'); ?>
<?= $form->field($model, 'notes')->label('Заметки о нем'); ?>

<div class="form-group">
	<?= Html::submitButton('Записать', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
