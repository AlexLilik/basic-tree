
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\ActiveRecord ;
use app\models\Staff ;

?>
<div class="jumbotron">
	<h2>Отдел кадров компании  'Забота'</h2>
	<p class="lead">Вы успешно добавили нового сотрудника !</p>
	<h5>(При необходимости его данные можно корректировать из общей структуры компании)</h5>
</div>

<?php


?>
<?=	DetailView::widget([
	'model' => $model,
	'attributes' => [
		//'id',
		'lastname',
		'firstname',
		'parentname',
		'position',
		'email:email',
		'homephone',
		'notes:ntext',
	],
]) ?>


<p style="text-align: center">
	<a class="btn btn-lg btn-success" href="index.php">Показать структуру</a>
</p>