<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Staff ;
use yii\db\ActiveRecord ;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Staffsearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="jumbotron">
    <h2>Отдел кадров компании  'Забота'</h2>
    <p class="lead">Список сотрудников компании для поиска и редактирования</p>
</div>
<div class="staff-index">


    <?= GridView::widget([

        'dataProvider' => $dataProvider,
       /* 'filterModel' => $searchModel,*/
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'lastname',
            'firstname',
            'position',
            'parentname',
            //'email:email',
            //'homephone',
            //'notes:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
