-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 04 2016 г., 22:31
-- Версия сервера: 5.5.45-log
-- Версия PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `basictree`
--

-- --------------------------------------------------------

--
-- Структура таблицы `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentid` int(11) DEFAULT NULL,
  `parentname` varchar(128) NOT NULL DEFAULT '''''',
  `firstname` varchar(50) NOT NULL DEFAULT '''''',
  `lastname` varchar(50) NOT NULL DEFAULT '''''',
  `position` varchar(50) NOT NULL DEFAULT '''''',
  `email` varchar(50) NOT NULL DEFAULT '''''',
  `homephone` varchar(50) NOT NULL DEFAULT '''''',
  `notes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Дамп данных таблицы `staff`
--

INSERT INTO `staff` (`id`, `parentid`, `parentname`, `firstname`, `lastname`, `position`, `email`, `homephone`, `notes`) VALUES
(44, -1, '''''', 'Иосиф', 'Сталин', 'Генеральный секретарь ЦК ВКП(б)', 'josyp@domain.com', '+001 001-01-01', 'Рельно крутой управленец. Проявляя заботу о людях может привести компанию к невиданному успеху'),
(45, 44, 'Сталин Иосиф  ::  Генеральный секретарь ЦК ВКП(б)', 'Вячеслав', 'Молотов', 'Народный Комиссар Иностранных дел', 'molot@domain.com', '+001 001 002', 'Отличный семьянин'),
(46, 49, 'Березин Иван  ::  Начальник первого управления разведки', 'Клим', 'Берия', 'Народный комиссар  обороны', 'klim@domain.com', '+001 001 002', 'старый конник'),
(47, 44, 'Сталин Иосиф  ::  Генеральный секретарь ЦК ВКП(б)', 'Владлен', 'Богач', 'Нарком НКВД', 'berezin@domain.com', '+001 001 002', ''),
(48, 46, 'Берия Клим  ::  Народный комиссар  обороны', 'Вячеслав', 'Емельяненко', 'Начальник контрразведки', 'ivan@domain.com', '+001 001 002', 'Отличный семьянин'),
(49, 45, 'Молотов Вячеслав  ::  Народный Комиссар Иностранных дел', 'Иван', 'Березин', 'Начальник первого управления разведки', 'ivan@domain.com', '+001 001 002', 'Отличный семьянин');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
