<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Staff ;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

	    if(!$staff = Staff::find()->all()) {
	        return $this->render('index');
        }else {
		    $model = new Staff() ;
		    return $this->render('structure',['model'=>$model] );// Строим дерево
	    }
    }


	/**
	 * Displays contact page.
	 *
	 * @return string
	 */
	public function actionAddemployee()
	{
		if(Staff::find()->all()) {

			$model = new Staff();

			if ($model->load(Yii::$app->request->post()) && $model->save()) {

				$model->parentname = Staff::getParentName($model->parentid) ;
				$model->save() ;

				return $this->render('addemployeeconfirm', ['model' => $model]);
			} else {

				return $this->render('addemployee', ['model' => $model]); // либо страница отображается первый раз, либо есть ошибка в данных
			}
		}else {
			return $this->render('index');
		}


	}
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionStructure()
    {
	    $model = new Staff() ;
	    return $this->render('structure',['model'=>$model] );// Строим дерево

    }


	public function actionBoss()
	{
		$model = new Staff();
		$model->parentid = -1 ;

		if ($model->load(Yii::$app->request->post())  && $model->save()) {

			return $this->render('bossconfirm', ['model' => $model]);
		} else {

			return $this->render('bossentry', ['model' => $model]); // либо страница отображается первый раз, либо есть ошибка в данных
		}
	}
}
